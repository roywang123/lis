package lis

import (
	"fmt"

	"github.com/spf13/cast"
)

type cases struct{}

var Cases cases

func (c *cases) getCaseBySampleNo(sample_no string) (record Mapping, err error) {
	return c.getCase(config.Mapping.SampleNo, sample_no)
}

func (c *cases) getCaseByCaseNo(case_no string) (record Mapping, err error) {
	return c.getCase(config.Mapping.CaseNo, case_no)
}

func (c *cases) getCase(k string, v string) (record Mapping, err error) {

	if k == "" {
		err = fmt.Errorf("the query keyword in the %s file is empty", INI_PATH)
		return
	}

	db, err := connect()
	if err != nil {
		return
	}

	lis_case := map[string]interface{}{}

	table_name := config.Database.TableName

	if table_name == "" {
		err = fmt.Errorf("the configuration for TableName in the %s file is empty", INI_PATH)
		return
	}

	db.Table(table_name).Where(fmt.Sprintf("%s = ?", k), v).Scan(&lis_case)

	if len(lis_case) == 0 {
		err = fmt.Errorf("matching nothing with %s: %s", k, v)
		return
	}

	record = formatLisCase(lis_case)
	return
}

func (c *cases) getCasesBySampleNos(sample_nos []string) (records []Mapping, err error) {
	return c.getCases(config.Mapping.SampleNo, sample_nos)
}

func (c *cases) getCasesByCaseNos(case_nos []string) (records []Mapping, err error) {
	return c.getCases(config.Mapping.CaseNo, case_nos)
}

func (c *cases) getCases(k string, v []string) (records []Mapping, err error) {

	if k == "" {
		err = fmt.Errorf("the query keyword in the %s file is empty", INI_PATH)
		return
	}

	db, err := connect()
	if err != nil {
		return
	}

	lis_cases := []map[string]interface{}{}

	table_name := config.Database.TableName

	if table_name == "" {
		err = fmt.Errorf("the configuration for TableName in the %s file is empty", INI_PATH)
		return
	}

	db.Table(table_name).Where(fmt.Sprintf("%s IN ?", k), v).Find(&lis_cases)

	if len(lis_cases) == 0 {
		err = fmt.Errorf("matching nothing with %s: %s", k, v)
		return
	}

	for _, lis_case := range lis_cases {
		records = append(records, formatLisCase(lis_case))
	}
	return
}

// 类型转换
func formatLisCase(lis_case map[string]interface{}) (r Mapping) {
	r = Mapping{
		CaseNo:             cast.ToString(lis_case[config.Mapping.CaseNo]),
		Sufferer:           cast.ToString(lis_case[config.Mapping.Sufferer]),
		Age:                cast.ToString(lis_case[config.Mapping.Age]),
		Sex:                cast.ToString(lis_case[config.Mapping.Sex]),
		Summary:            cast.ToString(lis_case[config.Mapping.Summary]),
		Provider:           cast.ToString(lis_case[config.Mapping.Provider]),
		ProviderPhone:      cast.ToString(lis_case[config.Mapping.ProviderPhone]),
		ProviderUnit:       cast.ToString(lis_case[config.Mapping.ProviderUnit]),
		CollectTime:        cast.ToString(lis_case[config.Mapping.CollectTime]),
		Disease:            cast.ToString(lis_case[config.Mapping.Disease]),
		Complaint:          cast.ToString(lis_case[config.Mapping.Complaint]),
		CaseHistory:        cast.ToString(lis_case[config.Mapping.CaseHistory]),
		Symptoms:           cast.ToString(lis_case[config.Mapping.Symptoms]),
		Diagnosis:          cast.ToString(lis_case[config.Mapping.Diagnosis]),
		Opinion:            cast.ToString(lis_case[config.Mapping.Opinion]),
		ReceivedDate:       cast.ToString(lis_case[config.Mapping.ReceivedDate]),
		SampleNo:           cast.ToString(lis_case[config.Mapping.SampleNo]),
		SampleType:         cast.ToString(lis_case[config.Mapping.SampleType]),
		InspectionItem:     cast.ToString(lis_case[config.Mapping.InspectionItem]),
		InspectionDate:     cast.ToString(lis_case[config.Mapping.InspectionDate]),
		InspectionMethod:   cast.ToString(lis_case[config.Mapping.InspectionMethod]),
		DetectionEquipment: cast.ToString(lis_case[config.Mapping.DetectionEquipment]),
	}
	return
}
