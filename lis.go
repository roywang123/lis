package lis

import (
	"errors"
	"fmt"
	"reflect"
)

type _case struct{}

var Case *_case

// 根据样本号获取病历信息
func (*_case) GetCaseBySampleNo(sample_no string) (data Mapping, err error) {
	err = loadConfig()
	if err != nil {
		return
	}
	data, err = Cases.getCaseBySampleNo(sample_no)
	if err != nil {
		err = fmt.Errorf("query failed: %v", err.Error())
		return
	}
	return
}

// 根据病历号获取病历信息
func (*_case) GetCaseByCaseNo(case_no string) (data Mapping, err error) {
	err = loadConfig()
	if err != nil {
		return
	}
	data, err = Cases.getCaseByCaseNo(case_no)
	if err != nil {
		err = fmt.Errorf("query failed: %v", err.Error())
		return
	}
	return
}

// 根据样本号列表获取病历信息
func (*_case) GetCasesBySampleNos(sample_nos []string) (data []Mapping, err error) {
	err = loadConfig()
	if err != nil {
		return
	}

	if reflect.TypeOf(sample_nos).String() != "[]string" {
		err = errors.New("parameter error: sample_nos is not array")
		return
	}

	var nos []string
	for _, sample_no := range sample_nos {
		if reflect.TypeOf(sample_no).String() != "string" {
			err = errors.New("parameter error: sample_no item is not string")
			return
		}
		nos = append(nos, sample_no)
	}

	data, err = Cases.getCasesBySampleNos(nos)
	if err != nil {
		err = fmt.Errorf("query failed: %v", err.Error())
		return
	}
	return
}

// 根据病例号列表获取病历信息
func (*_case) GetCasesByCaseNos(case_nos []string) (data []Mapping, err error) {
	err = loadConfig()
	if err != nil {
		return
	}

	if reflect.TypeOf(case_nos).String() != "[]string" {
		err = errors.New("parameter error: case_nos is not array")
		return
	}

	var nos []string
	for _, case_no := range case_nos {
		if reflect.TypeOf(case_no).String() != "string" {
			err = errors.New("parameter error: case_no item is not string")
			return
		}
		nos = append(nos, case_no)
	}

	data, err = Cases.getCasesByCaseNos(nos)
	if err != nil {
		err = fmt.Errorf("query failed: %v", err.Error())
		return
	}
	return
}
