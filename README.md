# Conf.ini配置文件说明

## System

系统配置

- [1] Port - 服务端口

## Files

文件存储

- [1] Reports - 病例报告存储路径

## Database

LIS数据库相关配置信息

- [1] Driver - 数据库类型(支持 oracle sqlserver mysql sqlite postgresql)
- [2] User - 登录名
- [3] Password - 密码
- [4] IP - 服务器地址
- [5] Port - 数据库端口
- [6] DBName - 数据库名称
- [7] TableName - 表名

## Mapping

Case病例表字段映射关系

以下涉及到的 22 个字段为本地系统病例表与LIS系统病例表的映射关系，以“患者姓名”为例进行说明：

### 映射关系

本地系统病例表字段 = LIS系统病例表

"Sufferer" = "PatientName"

"Sufferer" 为患者姓名在本地表中的字段名称

"PatientName" 为患者姓名在某医疗机构LIS系统表中字段名称

### 本地系统病例表字段

- [1] CaseNo - 病例号
- [2] Sufferer - 患者姓名
- [3] Age - 患者年龄
- [4] Sex - 患者性别
- [5] Summary - 病例摘要
- [6] Provider - 提供者姓名
- [7] ProviderPhone - 提供者电话
- [8] ProviderUnit - 提供者单位名称
- [9] CollectTime - 采集时间
- [10] Disease - 临床诊断
- [11] Complaint - 主诉
- [12] CaseHistory - 病史
- [13] Symptoms - 临床表现
- [14] Diagnosis - 综合诊断
- [15] Opinion - 综合意见
- [16] ReceivedDate - 接收日期
- [17] SampleNo - 样本编号
- [18] SampleType - 样本类型
- [19] InspectionItem - 送检项目
- [20] InspectionDate - 送检日期
- [21] InspectionMethod - 检测方法
- [22] DetectionEquipment - 检测设备