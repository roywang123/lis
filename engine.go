package lis

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

// Connect 返回一个gorm.DB对象
func connect() (db *gorm.DB, err error) {

	driver := config.Database.Driver
	user := config.Database.User
	pwd := config.Database.Password
	ip := config.Database.IP
	port := config.Database.Port
	dbName := config.Database.DBName

	var dsn string

	switch driver {
	case "sqlserver":
		dsn = fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s&encrypt=disable", user, pwd, ip, port, dbName)
		db, err = gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
	case "mysql":
		dsn = fmt.Sprintf("%s:%s@tcp(%s:%s)/dbname=%s", user, pwd, ip, port, dbName)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	case "postgresql":
		dsn = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", ip, user, pwd, dbName, port)
		db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	case "sqlite":
		dsn = fmt.Sprintf("%s.db", dbName)
		db, err = gorm.Open(sqlite.Open(dsn), &gorm.Config{})
		// case "oracle":
		// 	dsn = fmt.Sprintf("%s/%s@%s:%s/%s", user, pwd, ip, port, dbName)
		// 	db, err = gorm.Open(oracle.Open(dsn), &gorm.Config{})
	}

	if dsn == "" {
		err = fmt.Errorf("DB Driver Config Error: oracle sqlserver mysql sqlite postgresql are supported")
		return
	}
	if err != nil {
		err = fmt.Errorf("database link error: %v", err)
		return
	}
	return
}
