package lis

import (
	"fmt"
	"log"
	"path/filepath"

	"gopkg.in/ini.v1"
)

const (
	INI_PATH = "lis.Conf.ini"
)

// LIS 系统数据库相关配置
type Database struct {
	Driver    string `ini:"Driver"`    // 数据库类型
	User      string `ini:"User"`      // 登录名
	Password  string `ini:"Password"`  // 密码
	IP        string `ini:"IP"`        // 服务器地址
	Port      string `ini:"Port"`      // 数据库端口
	DBName    string `ini:"DBName"`    // 数据库名称
	TableName string `ini:"TableName"` // 表名
}

// 本地病例数据(相对固定不变)
type Mapping struct {
	CaseNo             string `ini:"CaseNo"`             // 病例号
	Sufferer           string `ini:"Sufferer"`           // 姓名
	Age                string `ini:"Age"`                // 年龄
	Sex                string `ini:"Sex"`                // 性别 1男 2女 0未知
	Summary            string `ini:"Summary"`            // 病例摘要
	Provider           string `ini:"Provider"`           // 提供者姓名
	ProviderPhone      string `ini:"ProviderPhone"`      // 提供者电话
	ProviderUnit       string `ini:"ProviderUnit"`       // 提供者单位名称
	CollectTime        string `ini:"CollectTime"`        // 采集时间
	Disease            string `ini:"Disease"`            // 临床诊断
	Complaint          string `ini:"Complaint"`          // 主诉
	CaseHistory        string `ini:"CaseHistory"`        // 病史
	Symptoms           string `ini:"Symptoms"`           // 临床表现
	Diagnosis          string `ini:"Diagnosis"`          // 综合诊断
	Opinion            string `ini:"Opinion"`            // 综合意见
	ReceivedDate       string `ini:"ReceivedDate"`       // 接收日期
	SampleNo           string `ini:"SampleNo"`           // 样本编号
	SampleType         string `ini:"SampleType"`         // 样本类型
	InspectionItem     string `ini:"InspectionItem"`     // 送检项目
	InspectionDate     string `ini:"InspectionDate"`     // 送检日期
	InspectionMethod   string `ini:"InspectionMethod"`   // 检测方法
	DetectionEquipment string `ini:"DetectionEquipment"` // 检测设备
	Physician          string `ini:"Physician"`          // 检查医师
	AdmissionNo        string `ini:"AdmissionNo"`        // 住院号
}

type conf struct {
	System struct {
		Port int `ini:"Port"` // web 端口
	}
	Files struct {
		Reports string `ini:"Reports"` // 报告存储文件夹
	}
	// LIS 系统数据库相关配置
	Database
	// 本地病例数据(相对固定不变)
	Mapping
}

var config = conf{}

func loadConfig() (err error) {

	cfg, err := loadIniFile()
	if err != nil {
		return
	}

	err = cfg.MapTo(&config)

	if err != nil {
		err = fmt.Errorf("config file format error: %v", err.Error())
		return
	}
	return
}

func loadIniFile() (cfg *ini.File, err error) {
	ini_path, err := filepath.Abs(fmt.Sprintf("./%s", INI_PATH))
	if err != nil {
		err = fmt.Errorf("get config file path error: %v", err.Error())
		return
	}
	cfg, err = ini.Load(ini_path)
	if err != nil {
		log.Fatalln("load config error.")
		err = fmt.Errorf("load config error: %v", err.Error())
		return
	}
	return
}
