package lis

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type report struct{}

var Report *report

/*
上传pdf报告文件

	input:
		port: 系统端口号
		system_type: 系统类型(cell, karyo ...)
		sample_no: 样本号
		src_path: 报告源文件路径
*/
func (*report) Upload(port, system_type, sample_no, src_path string) (err error) {

	if port == "" || system_type == "" || sample_no == "" || src_path == "" {
		err = errors.New("parameter error")
		return
	}

	// 根目录
	root_dir, _ := filepath.Abs(config.Files.Reports)
	// 第一级目录：Cell-8001
	first_dir := system_type + "-" + port
	// 第二级目录：2023-01(截取样本序号yyyy-mm)
	secondary_dir := strings.Join(strings.Split(sample_no, "-")[:2], "-")
	// 第三级目录：样本序号(2023-03-1-04236)

	// 拼接文件路径
	report_file_path := filepath.Join(root_dir, first_dir, secondary_dir, sample_no) + path.Ext(src_path)

	src_report, err := os.ReadFile(src_path)

	if err != nil {
		err = fmt.Errorf("read source report error: %v", err.Error())
		return
	}

	err = os.WriteFile(report_file_path, src_report, 6044)
	if err != nil {
		err = fmt.Errorf("report copy error: %v", err.Error())
		return
	}

	return
}
